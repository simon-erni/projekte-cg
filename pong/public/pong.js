"use strict";
var canvas;
var gl;
var aVertexPositionId;
var aColorPositionId;
var buffer;

var uModelViewMatrixId;
var ball;
var paddleL;
var paddleR;
var middleLine;
var rectangles = [];

var shaderProgram;
var currentTimeStamp;
var gameOver = true;

var socket = io();

function start() {
    canvas = document.getElementById("pong");
    gl = createGLContext(canvas);

    initObjects();
    initGL();
    drawAnimated(0);

}


socket.on('game', function (msg) {

    if (msg === 'start') {
        resetGameOver();
    }
    if (msg === 'stop') {
        gameOver = true;
    }

});

function startGame() {
    socket.emit('game', 'start');

}

function stopGame() {
    socket.emit('game', 'stop');
}

function resetGameOver() {
    initObjects();
    gameOver = false;
}


function updatePaddleDirection(paddle, direction, emitViaSocket) {
    if (direction === 0) {
        paddle.timePressed = 0;
        paddle.posPressed = 0;
    } else {
        paddle.timePressed = currentTimeStamp;
        paddle.posPressed = paddle.y;
    }
    paddle.direction = direction;
    if (emitViaSocket) {
        socket.emit(paddle.name, {direction: paddle.direction, position: paddle.x});
    }
}

socket.on('paddleL', function (msg) {
    updatePaddleDirection(paddleL, msg.direction, false);
    paddleL.x = msg.position;

});
socket.on('paddleR', function (msg) {
    updatePaddleDirection(paddleR, msg.direction, false);
    paddleR.x = msg.position;

});

/**
 * 38: up arrow
 * 40: down arrow
 *
 * 87: W
 * 83: S
 */
document.addEventListener('keydown', function (event) {

    if (event.keyCode === 87 && paddleL.direction !== 1) {
        updatePaddleDirection(paddleL, 1, true);
    }
    else if (event.keyCode === 83) {
        updatePaddleDirection(paddleL, -1, true);
    }

    if (event.keyCode === 38 && paddleR.direction !== 1) {
        updatePaddleDirection(paddleR, 1, true);
    }
    else if (event.keyCode === 40) {
        updatePaddleDirection(paddleR, -1, true);
    }

    if (event.keyCode === 32) {
        startGame();
    }

});

document.addEventListener('keyup', function (event) {


    if (event.keyCode === 87 && paddleL.direction !== -1 || event.keyCode === 83 && paddleL.direction !== 1) {
        updatePaddleDirection(paddleL, 0, true);
    }

    if (event.keyCode === 38 && paddleR.direction !== -1 || event.keyCode === 40 && paddleR.direction !== 1) {
        updatePaddleDirection(paddleR, 0, true);
    }

});


function moveObjects(time) {


    if (paddleL.direction !== 0) {
        paddleL.y = paddleL.posPressed + (time - paddleL.timePressed) * paddleL.speed * paddleL.direction;
    }

    if (paddleR.direction !== 0) {
        paddleR.y = paddleR.posPressed + (time - paddleR.timePressed) * paddleR.speed * paddleR.direction;
    }

    ball.x += ball.xSpeed;
    ball.y += ball.ySpeed;

    if (ball.x <= paddleL.x + paddleL.width) {

        if (paddleL.y <= ball.y && ball.y <= paddleL.y + paddleL.height) {
            ball.x = paddleL.x + paddleL.width;
            ball.xSpeed = 0 - ball.xSpeed;
        } else {
            gameOver = true;
            paddleR.points++;
        }
    }

    if (ball.x >= paddleR.x ) {

        if (paddleR.y <= ball.y && ball.y <= paddleR.y + paddleR.height) {
            ball.x = paddleR.x;
            ball.xSpeed = 0 - ball.xSpeed;

        } else {
            stopGame();
            paddleL.points++;
        }
    }


    if (ball.y <= 0 || ball.y >= canvas.height - ball.height) {
        ball.ySpeed = 0 - ball.ySpeed;
    }
}

function initObjects() {

    ball = {
        xSpeed: 5,
        ySpeed: 5,
        width: 10,
        height: 10,
        x: canvas.width / 2 - 50,
        y: canvas.height / 2 - 5,
        color: [1.0, 1.0, 1.0, 1.0]
    };

    paddleL = {
        name: 'paddleL',
        points: 0,
        speed: 0.4,
        posPressed: 0,
        timePressed: 0,
        direction: 0,
        width: 10,
        height: 100,
        x: 50,
        y: canvas.height / 2 - 50,
        color: [1.0, 1.0, 1.0, 1.0]
    };

    paddleR = {
        name: 'paddleR',
        points: 0,
        speed: 0.4,
        posPressed: 0,
        timePressed: 0,
        direction: 0,
        width: 10,
        height: 100,
        x: canvas.width - 50,
        y: canvas.height / 2 - 50,
        color: [1.0, 1.0, 1.0, 1.0]
    };
    middleLine = {
        width: 2,
        height: canvas.height,
        x: canvas.width / 2 - 1,
        y: 0,
        color: [1.0, 1.0, 1.0, 1.0]
    };

    rectangles = [];


    rectangles.push(ball);
    rectangles.push(paddleL);
    rectangles.push(paddleR);
    rectangles.push(middleLine);

}
function drawAnimated(time) {

    currentTimeStamp = time;

    draw();

    if (!gameOver) {
        moveObjects(time);
    }
    drawObjects();


    window.requestAnimationFrame(drawAnimated);


}

function drawObjects() {
    for (var i = 0; i < rectangles.length; i++) {
        drawRectangle(rectangles[i]);
    }
}


function createGLContext(canvas) {
    // get the gl drawing context
    var context = canvas.getContext("webgl");

    if (!context) {
        alert("Failed to create GL context");
    }

    return WebGLDebugUtils.makeDebugContext(context);
}


function initGL() {
    var fragmentShader = getShader(gl, "shader-fs");
    var vertexShader = getShader(gl, "shader-vs");

    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert(" Failed to setup shader ");
        return;
    }
    gl.useProgram(shaderProgram);


}

function drawRectangle(rectangle) {

    var vertices = [
        rectangle.x, rectangle.y, rectangle.color[0], rectangle.color[1], rectangle.color[2], rectangle.color[3],
        rectangle.x + rectangle.width, rectangle.y, rectangle.color[0], rectangle.color[1], rectangle.color[2], rectangle.color[3],
        rectangle.x, rectangle.y + rectangle.height, rectangle.color[0], rectangle.color[1], rectangle.color[2], rectangle.color[3],
        rectangle.x + rectangle.width, rectangle.y + rectangle.height, rectangle.color[0], rectangle.color[1], rectangle.color[2], rectangle.color[3],
        rectangle.x, rectangle.y, rectangle.color[0], rectangle.color[1], rectangle.color[2], rectangle.color[3]
    ];

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.clearColor(0, 0, 0, 1.0);

    var mFinal = mat4.create();


    var mScaling = mat4.create();

    mScaling[0] = 2.0 / canvas.width;
    mScaling[5] = 2.0 / canvas.height;

    var vTrans = vec3.fromValues(-1, -1, 0);
    var mTrans = mat4.create();
    mTrans = mat4.fromTranslation(mTrans, vTrans);

    mFinal = mat4.multiply(mFinal, mTrans, mScaling);

    buffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);

    setupAttributes();

    gl.uniformMatrix4fv(uModelViewMatrixId, false, mFinal);

}

function setupAttributes() {
    aVertexPositionId = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    gl.vertexAttribPointer(aVertexPositionId, 2, gl.FLOAT, false, 24, 0);
    gl.enableVertexAttribArray(aVertexPositionId);

    aColorPositionId = gl.getAttribLocation(shaderProgram, "aColor");
    gl.vertexAttribPointer(aColorPositionId, 4, gl.FLOAT, false, 24, 8);
    gl.enableVertexAttribArray(aColorPositionId);

    uModelViewMatrixId = gl.getUniformLocation(shaderProgram, "uModelViewMatrix");
}


//
// getShader
//
// Loads a shader program by scouring the current document,
// looking for a script with the specified ID.
//
function getShader(gl, id) {
    var shaderScript = document.getElementById(id);

    // Didn't find an element with the specified ID; abort.

    if (!shaderScript) {
        return null;
    }

    // Walk through the source element's children, building the
    // shader source string.

    var theSource = "";
    var currentChild = shaderScript.firstChild;

    while (currentChild) {
        if (currentChild.nodeType === 3) {
            theSource += currentChild.textContent;
        }

        currentChild = currentChild.nextSibling;
    }

    // Now figure out what type of shader script we have,
    // based on its MIME type.

    var shader;

    if (shaderScript.type === "x-shader/x-fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (shaderScript.type === "x-shader/x-vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
        return null;  // Unknown shader type
    }

    // Send the source to the shader object

    gl.shaderSource(shader, theSource);

    // Compile the shader program

    gl.compileShader(shader);

    // See if it compiled successfully

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader));
        return null;
    }

    return shader;
}