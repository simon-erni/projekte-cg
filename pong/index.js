var app = require('express')();
var http = require('http').Server(app);
var express = require('express');
var io = require('socket.io')(http);


io.on('connection', function (socket) {
    socket.on('paddleL', function (msg) {

        io.emit('paddleL', msg);
    });

    socket.on('paddleR', function (msg) {

        io.emit('paddleR', msg);
    });

    socket.on('game', function (msg) {
        io.emit('game', msg);

    });
});


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.use(express.static(__dirname + '/public'));

http.listen(3000, function () {
    console.log('listening on *:3000');
});