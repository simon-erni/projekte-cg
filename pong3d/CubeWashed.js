/**
 * Created by zakoller on 12.11.14.
 * V2: 22.10.2015
 *
 * Define and draw a colored cube.
 */
/*jslint node: true */
"use strict";

/**
 * Define the vertices of the cube
 */
function defineWashedCubeVertices(gl) {
    // define the vertices of the cube
    // we organize them now by side, so that we can have constant colored faces
    var vertices = [
        // back
        0, 0, 0,      // v0
        1, 0, 0,      // v1
        1, 1, 0,      // v2
        0, 1, 0,      // v3
                      // front
        0, 0, 1,      // v4
        1, 0, 1,      // v5
        1, 1, 1,      // v6
        0, 1, 1      // v7
                      // right

    ];
    var bufferVertices  = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferVertices);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    return bufferVertices;
}

function defineWashedCubeColors(gl) {
    var color = [
        1.0, 0.0, 0.0,
        1.0, 1.0, 0.0,
        0.0, 1.0, 1.0,
        1.0, 0.0, 1.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0,
        1.0, 0.5, 0.5,
        0.0, 1.0, 0.5];

    var bufferColor = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferColor);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(color), gl.STATIC_DRAW);
    return bufferColor;
}

function defineWashedCubeSides(gl) {
    var vertexIndices = [
        0,2,1, // face 0 (back)
        2,0,3,
        4,5,6, // face 1 (front)
        4,6,7,
        5,1,6, // face 2 (right)
        1,2,6,
        0,4,7, // face 3 (left)
        0,7,3,
        2,3,6, // face 4 (top)
        3,7,6,
        0,1,5, // face 5 (bottom)
        0,5,4
    ];
    var bufferSides = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, bufferSides);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vertexIndices), gl.STATIC_DRAW);
    return bufferSides;
}

/**
 * Define the cube, the cube is modelled at the origin
 */
function defineWashedCube(gl) {
    return {
        bufferVertices: defineWashedCubeVertices(gl),
        bufferColor: defineWashedCubeColors(gl),
        bufferSides: defineWashedCubeSides(gl)
    }
}

/**
 * Draw the cube.
 */
function drawWashedCube(gl, cube, aVertexPositionId, aVertexColorId, uniformMatrixId, matrixIn) {
    // setup transformation
    var matrix = mat4.create();
    // translate so that the position is the centre of the cube
    mat4.translate(matrix, matrixIn, vec3.fromValues(-0.5, -0.5, -0.5));

    gl.uniformMatrix4fv(uniformMatrixId, false, matrix);

    // bind the buffer, so that the cube vertices are used
    gl.bindBuffer(gl.ARRAY_BUFFER, cube.bufferVertices);
    gl.vertexAttribPointer(aVertexPositionId, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(aVertexPositionId);

    // bind the buffer for color
    gl.bindBuffer(gl.ARRAY_BUFFER, cube.bufferColor);
    gl.vertexAttribPointer(aVertexColorId, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(aVertexColorId);

    // draw the elements
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cube.bufferSides);
    gl.drawElements(gl.TRIANGLES, 36 ,gl.UNSIGNED_SHORT, 0);
}

