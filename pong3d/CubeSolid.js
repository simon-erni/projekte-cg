/**
 * Created by zakoller on 12.11.14.
 * V2: 22.10.2015
 *
 * Define and draw a colored cube.
 */
/*jslint node: true */
"use strict";

/**
 * Define the vertices of the cube
 */
function defineSolidCubeVertices(gl) {
    // define the vertices of the cube
    // we organize them now by side, so that we can have constant colored faces
    var vertices = [
        // back
        0, 0, 0,      // v0
        1, 0, 0,      // v1
        1, 1, 0,      // v2
        0, 1, 0,      // v3
                      // front
        0, 0, 1,      // v4
        1, 0, 1,      // v5
        1, 1, 1,      // v6
        0, 1, 1,      // v7
                      // right
        1, 0, 0,      // v8 = v1
        1, 1, 0,      // v9 = v2
        1, 1, 1,      // v10 = v6
        1, 0, 1,      // v11 = v5
                      // left
        0, 0, 0,      // v12 = v0
        0, 1, 0,      // v13 = v3
        0, 1, 1,      // v14 = v7
        0, 0, 1,      // v15 = v4
                      // top
        0, 1, 0,      // v16 = v3
        0, 1, 1,      // v17 = v7
        1, 1, 1,      // v18 = v6
        1, 1, 0,      // v19 = v2
                      //bottom
        0, 0, 0,      // v20 = v0
        0, 0, 1,      // v21 = v4
        1, 0, 1,      // v22 = v5
        1, 0, 0       // v23 = v1
    ];
    var bufferVertices  = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferVertices);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    return bufferVertices;
}

function defineSolidCubeColors(gl) {
    var backColor = [1.0, 0.0, 0.0],
        frontColor = [0.0, 0.0, 1.0],
        rightColor = [0.0, 1.0, 0.0],
        leftColor = [1.0, 1.0, 0.0],
        topColor = [1.0, 0.0, 1.0],
        bottomColor = [0.0, 1.0, 1.0];

    // make 4 entries, one for each vertex
    var backSide    = backColor.concat(backColor, backColor, backColor);
    var frontSide   = frontColor.concat(frontColor, frontColor, frontColor);
    var rightSide   = rightColor.concat(rightColor, rightColor, rightColor);
    var leftSide    = leftColor.concat(leftColor, leftColor, leftColor);
    var topSide     = topColor.concat(topColor, topColor, topColor);
    var bottomSide  = bottomColor.concat(bottomColor, bottomColor, bottomColor);

    var allSides = backSide.concat(frontSide, rightSide, leftSide, topSide, bottomSide);

    var bufferColor = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferColor);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(allSides), gl.STATIC_DRAW);
    return bufferColor;
}

function defineSolidCubeSides(gl) {
    var vertexIndices = [
        0,2,1, // face 0 (back)
        2,0,3,
        4,5,6, // face 1 (front)
        4,6,7,
        8,9,10, // face 2 (right)
        10,11,8,
        12,15,14, // face 3 (left)
        14,13,12,
        16,17,18, // face 4 (top)
        18,19,16,
        20,23,22, // face 5 (bottom)
        22,21,20
    ];
    var bufferSides = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, bufferSides);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vertexIndices), gl.STATIC_DRAW);
    return bufferSides;
}

/**
 * Define the cube, the cube is modelled at the origin
 */
function defineSolidCube(gl) {
    return {
        bufferVertices: defineSolidCubeVertices(gl),
        bufferColor: defineSolidCubeColors(gl),
        bufferSides: defineSolidCubeSides(gl)
    }
}

/**
 * Draw the cube.
 */
function drawSolidCube(gl, cube, aVertexPositionId, aVertexColorId, uniformMatrixId, matrixIn) {
    // setup transformation
    var matrix = mat4.create();
    // translate so that the position is the centre of the cube
    mat4.translate(matrix, matrixIn, vec3.fromValues(-0.5, -0.5, -0.5));

    gl.uniformMatrix4fv(uniformMatrixId, false, matrix);

    // bind the buffer, so that the cube vertices are used
    gl.bindBuffer(gl.ARRAY_BUFFER, cube.bufferVertices);
    gl.vertexAttribPointer(aVertexPositionId, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(aVertexPositionId);

    // bind the buffer for color
    gl.bindBuffer(gl.ARRAY_BUFFER, cube.bufferColor);
    gl.vertexAttribPointer(aVertexColorId, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(aVertexColorId);

    // draw the elements
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cube.bufferSides);
    gl.drawElements(gl.TRIANGLES, 36 ,gl.UNSIGNED_SHORT, 0);
}

