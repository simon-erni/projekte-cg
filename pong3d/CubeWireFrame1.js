/**
 * Created by zakoller on 10.11.14.
 *
 * Computer Graphics Uebung 3
 *
 * Definition of WireFrameCube
 */
"use strict";

/**
 * Define the cube, the cube is modelled at the origin
 */
function defineWireFrameCube(gl) {
    return {
        bufferVertices: defineWireFrameCubeVertices(gl),
        bufferEdges: defineWireFrameCubeEdges(gl)
    };
}

function defineWireFrameCubeVertices(gl) {
    // define the vertices of the cube
    var vertices = [
        0,0,0,      // v0
        1,0,0,      // v1
        1,1,0,      // v2
        0,1,0,      // v3
        0,0,1,      // v4
        1,0,1,      // v5
        1,1,1,      // v6
        0,1,1      // v7
    ];
    var bufferVertices  = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferVertices);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    return bufferVertices;
}

function defineWireFrameCubeEdges(gl) {
    // define the edges for the cube, there are 12 edges in a cube
    var vertexIndices = [
        0,1, // face on z = 0
        1,2,
        2,3,
        3,0,
        4,5, // face on z = 1
        5,6,
        6,7,
        7,4,
        0,4, // edges between
        1,5,
        2,6,
        3,7
    ];
    var bufferEdges = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, bufferEdges);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vertexIndices), gl.STATIC_DRAW);
    return bufferEdges;
}

/**
 * Draw the cube.
 */
function drawWireFrameCube(gl, cube, aVertexPositionId, uModelViewMatrixId, matrixIn) {
    var matrix = mat4.create();
    // translate so that the position is the centre of the cube
    mat4.translate(matrix, matrixIn, vec3.fromValues(-0.5, -0.5, -0.5));

    gl.uniformMatrix4fv(uModelViewMatrixId, false, matrix);

    // bind the buffer, so that the ball vertices are used
    gl.bindBuffer(gl.ARRAY_BUFFER, cube.bufferVertices);
    gl.vertexAttribPointer(aVertexPositionId, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(aVertexPositionId);

    // bind the element array
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cube.bufferEdges);
    gl.drawElements(gl.LINES, 24 ,gl.UNSIGNED_SHORT, 0);
}
