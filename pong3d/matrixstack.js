
function MatrixStack() {
    "use strict";
    this.stack = [];
}

MatrixStack.prototype.push = function(matrix) {
    "use strict";
    this.stack.push(matrix);
};

MatrixStack.prototype.pop = function() {
    "use strict";
    return this.stack.pop();
};

MatrixStack.prototype.top = function() {
    "use strict";
    var index = this.stack.length - 1;
    if (index < 0) {
        console.log("Error in MatrixStack.top");
    }
    return mat4.clone(this.stack[index]);
};


