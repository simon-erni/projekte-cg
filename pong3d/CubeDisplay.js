/**
 * Created by zakoller on 10.10.14.
 *
 * Computer Graphics Uebung 3
 *
 * Version 2, Filled colored cube
 */
"use strict";

// we save the canvas and the gl context globally
var canvas;
var gl;

// program and attribute locations
var shaderProgram;

// attribute locations
var aVertexPositionId;
var aVertexColorId;
var uModelViewMatrixId;
var uProjectionMatrixId;

var projectionMatrix;

var alpha = 0.0;
var alphaVelocity = 0.001;

// objects to draw
var solidCube;
var wireFrameCube;
var washedCube;


/**
 * startup function to be called when the body is loaded
 */
function startup() {
	canvas = document.getElementById("gameCanvas");
	gl = createGLContext(canvas);
	initGL();
	draw();
    window.requestAnimationFrame(drawAnimated);

}

/**
 * Initialize openGL. Only called once to setup appropriate parameters.
 */
function initGL(){
    gl.clearColor(0.2, 0.2, 0.2, 1.0);
    gl.enable(gl.DEPTH_TEST);

    //gl.frontFace(gl.CCW);       // defines how the front face is drawn
    //gl.cullFace(gl.BACK);       // defines which face should be culled
    //gl.enable(gl.CULL_FACE);    // enables culling

	initShaders();
    initCamera();
	setupAttributes();
	setupObjects();
}

/**
 * Initialize the camera
 */
function initCamera() {
    // init model view matrix
    var matrix = mat4.create();
    mat4.identity(matrix);
    mat4.lookAt(matrix, [0.0,0.6,3],[0, 0, 0], [0, 1, 0]);
    pushMatrix(matrix);

    projectionMatrix = mat4.create();
    // init projection matrix
    var aspectRatio = gl.drawingBufferWidth / gl.drawingBufferHeight;
    //mat4.ortho(projectionMatrix, -aspectRatio,aspectRatio, -1.0, 1.0, 0.5, 10.0);
    //mat4.frustum(projectionMatrix, -aspectRatio, aspectRatio, -1.0, 1.0,  0.5, 10.0);
    mat4.perspective(projectionMatrix, glMatrix.toRadian(40), aspectRatio,  0.5, 10.0);
}

/**
 * Initialize the shader programs.
 */
function initShaders() {
    var fragmentShader = getShader(gl, "shader-fs");
    var vertexShader = getShader(gl, "shader-vs");

	shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);

	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		alert("Failed to setup shader");
		return;
	}
	gl.useProgram(shaderProgram);
}

/**
 * Create and return the WebGL context 
 */
function createGLContext(canvas) {
	// get the gl drawing context
	var context = canvas.getContext("webgl");
	if (!context) {
		alert("Failed to create GL context");
	}
	// wrap the context to a debug context to get error messages
	return WebGLDebugUtils.makeDebugContext(context);
}

/**
 * Setup the location of the attributes for communication with the shaders
 */
function setupAttributes() {
	// finds the index of the variable in the program
	aVertexPositionId = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    aVertexColorId = gl.getAttribLocation(shaderProgram, "aVertexColor");
    uModelViewMatrixId = gl.getUniformLocation(shaderProgram, "uModelViewMatrix");
    uProjectionMatrixId = gl.getUniformLocation(shaderProgram, "uProjectionMatrix");
}

/**
 * Setup the objects to be drawn
 */
function setupObjects() {
	solidCube = defineSolidCube(gl);
    wireFrameCube = defineWireFrameCube(gl);
    washedCube = defineWashedCube(gl);
}

/**
 * Draw the scene
 */
function draw() {
	//gl.clear(gl.COLOR_BUFFER_BIT);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.viewport(0, 0, canvas.clientWidth, canvas.clientHeight);
    gl.uniformMatrix4fv(uProjectionMatrixId, false, projectionMatrix);
    var matrix = topMatrix();
    mat4.translate(matrix, matrix, [-0.5, 0.0, 0.0]);
    mat4.rotateY(matrix, matrix, alpha);
    //drawCube(gl, aVertexPositionId, uModelViewMatrixId, matrix);
    mat4.scale(matrix, matrix, [0.5, 0.5, 0.5]);
    drawSolidCube(gl, solidCube, aVertexPositionId, aVertexColorId, uModelViewMatrixId, matrix);

    matrix = topMatrix();
    mat4.translate(matrix, matrix, [0.5, 0.0, 0.0]);
    mat4.rotateY(matrix, matrix, -alpha);
    mat4.scale(matrix, matrix, [0.5, 0.5, 0.5]);
    drawWashedCube(gl, washedCube, aVertexPositionId, aVertexColorId, uModelViewMatrixId, matrix)
}

var first = false;
var lastTimeStamp = 0;
function drawAnimated(timeStamp) {
    if (first) {
        lastTimeStamp = timeStamp;
        first = false;
    } else {
        var timeElapsed = timeStamp - lastTimeStamp;
        lastTimeStamp = timeStamp;
        alpha += alphaVelocity * timeElapsed;
        if (alpha > 2*Math.PI) {
            alpha -= 2*Math.PI;
        }
    }
    draw();
    window.requestAnimationFrame(drawAnimated);
}
// this variable should only be used here for implementing the matrix stack
var modelViewMatrixStack = [];

/**
 * Push the matrix to the stack
 * @param matrix the matrix to be pushed
 */
function pushMatrix(matrix) {
    modelViewMatrixStack.push(matrix);
}

/**
 *
 * @returns the matrix that was on top of the stack
 */
function popMatrix() {
    return modelViewMatrixStack.pop();
}

/**
 * Returns a copy of the matrix at the top of the stack. This matrix can then be manipulated
 * without an effect upon other matrices on the stack
 * @returns {mat4} A copy of the matrix on top of the stack.
 */
function topMatrix() {
    var matrix = modelViewMatrixStack[modelViewMatrixStack.length - 1];
    return mat4.clone(matrix);
}


/**
 * Loads a shader program by scouring the current document, looking for a script with specified Id.
 * @param gl
 * @param id
 * @returns {*}
 */
function getShader(gl, id) {
    var shaderScript = document.getElementById(id);

    // Didn't find an element with the specified ID; abort.

    if (!shaderScript) {
        return null;
    }

    // Walk through the source element's children, building the
    // shader source string.

    var theSource = "";
    var currentChild = shaderScript.firstChild;

    while (currentChild) {
        if (currentChild.nodeType === 3) {
            theSource += currentChild.textContent;
        }

        currentChild = currentChild.nextSibling;
    }

    // Now figure out what type of shader script we have,
    // based on its MIME type.

    var shader;

    if (shaderScript.type === "x-shader/x-fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (shaderScript.type === "x-shader/x-vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
        return null;  // Unknown shader type
    }

    // Send the source to the shader object

    gl.shaderSource(shader, theSource);

    // Compile the shader program

    gl.compileShader(shader);

    // See if it compiled successfully

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader));
        return null;
    }

    return shader;
}